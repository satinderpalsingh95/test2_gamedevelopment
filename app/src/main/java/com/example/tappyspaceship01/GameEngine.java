package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    int playerXPosition;
    int playerYPosition;
    Bitmap playerImage;
    Rect playerHitbox;
    Player player;
    Item item;
    Item item1;
    Item item2;



    int itemXPosition;
    int itemYPosition;
    Bitmap itemImage;
    Rect itemHitbox;

    int score = 0;
    int livesRemaining = 3;

    private int randomLane;
    private int randomItem;

    Random r = new Random();

    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();
        this.player = new Player(getContext(),w-180,h-500);

        randomLane = r.nextInt(4);
        randomItem = r.nextInt(3);
        System.out.println(randomItem);
        switch (randomLane)
        {
            case 0:
                this.itemXPosition= 0;
                this.itemYPosition=0;
            case 1:
                this.itemXPosition= 0;
                this.itemYPosition=130;
            case 2:
                this.itemXPosition= 0;
                this.itemYPosition=260;
            case 3:
                this.itemXPosition= 0;
                this.itemYPosition=390;
        }
        switch (randomItem)
        {
            case 0:
                this.itemImage = BitmapFactory.decodeResource(this.getContext().getResources(),
                        R.drawable.candy64);
            case 1:
                this.itemImage = BitmapFactory.decodeResource(this.getContext().getResources(),
                        R.drawable.poop64);
            case 2:
                this.itemImage = BitmapFactory.decodeResource(this.getContext().getResources(),
                        R.drawable.rainbow64);

        }
        this.item = new Item(getContext(),itemXPosition,itemYPosition);
        //this.itemHitbox = new Rect(itemXPosition,itemYPosition,itemXPosition+itemImage.getWidth(),itemYPosition+itemImage.getHeight());





    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

        //this.itemXPosition += 20;
        this.item.setxPosition(item.getxPosition()+20);
        player.updateHitbox();
        item.updateHitbox();

        if(player.getHitbox().intersect(item.getHitbox())){
            score+=1;
            randomLane = r.nextInt(4);
            randomItem = r.nextInt(3);
            switch (randomLane)
            {
                case 0:
                    this.itemXPosition= 0;
                    this.itemYPosition=0;
                case 1:
                    this.itemXPosition= 0;
                    this.itemYPosition=130;
                case 2:
                    this.itemXPosition= 0;
                    this.itemYPosition=260;
                case 3:
                    this.itemXPosition= 0;
                    this.itemYPosition=390;
            }
            switch (randomItem)
            {
                case 0:
                    this.item.setImage(BitmapFactory.decodeResource(this.getContext().getResources(),
                            R.drawable.candy64));
                case 1:
                    this.item.setImage(BitmapFactory.decodeResource(this.getContext().getResources(),
                            R.drawable.rainbow64));
                case 2:
                    this.item.setImage(BitmapFactory.decodeResource(this.getContext().getResources(),
                            R.drawable.poop64));

            }
            item.setxPosition(itemXPosition);
            item.setyPosition(itemYPosition);
            this.item = new Item(getContext(),itemXPosition,itemYPosition);
            //livesRemaining-=1;
        }

    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.BLACK);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLACK);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(3);


            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);

            //player.getImage();
            // draw player graphic on screen
            //canvas.drawBitmap(playerImage, playerXPosition, playerYPosition, paintbrush);
            canvas.drawBitmap(item.getImage(),item.getxPosition(),item.getyPosition(),paintbrush);
            // draw the player's hitbox
            //canvas.drawRect(this.playerHitbox, paintbrush);
            canvas.drawRect(item.getHitbox(), paintbrush);
            this.canvas.drawRect(0,130,980,140,paintbrush);
            this.canvas.drawRect(0,260,980,270,paintbrush);
            this.canvas.drawRect(0,390,980,400,paintbrush);
            this.canvas.drawRect(0,520,980,530,paintbrush);


            // game stats
            paintbrush.setTextSize(30);
            canvas.drawText("Lives = " + livesRemaining,
                    100,
                    screenHeight-170,
                    paintbrush
            );
            canvas.drawText("Score = " + score,
                    300,
                    screenHeight-170,
                    paintbrush
            );

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        float xposfinger = event.getX();
        float yposfinger = event.getY();
         float screenMiddle = screenHeight/2;
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            if(yposfinger<=screenMiddle)
            {
                if(player.getyPosition()+player.getImage().getHeight()<= 200)
                {

                }
                else{
                    player.setyPosition(player.getyPosition()-130);
                }


            }
            else
            {
                if(player.getyPosition()+player.getImage().getHeight()>= 500)
                {

                }
                else {
                    player.setyPosition(player.getyPosition() + 130);
                }
            }
            fingerAction = "mousedown";
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            fingerAction = "mouseup";
        }

        return true;
    }
}
